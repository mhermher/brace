context("Brace Object")

test_that(
    "Unopened Case Key Only",
    {
        expect_is(
            Sentence("State STATES} included"),
            "Sentence"
        )
        expect_is(
            Sentence("State STATES} included")$root(),
            "Conjunction"
        )
        expect_is(
            Sentence("State STATES} included")$root()$step(),
            "NULL"
        )
        expect_equal(
            Sentence("State STATES} included")$print(),
            "State STATES} included"
        )
        expect_equal(
            Sentence("State STATES} included")$root()$print(),
            "State STATES} included"
        )
    }
)

test_that(
    "Unsealed Case Key Only",
    {
        expect_is(
            Sentence("State {STATES included"),
            "Sentence"
        )
        expect_is(
            Sentence("State {STATES included")$root(),
            "Conjunction"
        )
        expect_is(
            Sentence("State {STATES included")$root()$step(),
            "NULL"
        )
        expect_equal(
            Sentence("State {STATES included")$print(),
            "State {STATES included"
        )
        expect_equal(
            Sentence("State {STATES included")$root()$print(),
            "State {STATES included"
        )
    }
)

test_that(
    "Correct Case Key Only",
    {
        expect_is(
            Sentence("State {STATES} included"),
            "Sentence"
        )
        expect_is(
            Sentence("State {STATES} included")$root(),
            "Conjunction"
        )
        expect_is(
            Sentence("State {STATES} included")$root()$step(),
            "Clause"
        )
        expect_is(
            Sentence("State {STATES} included")$root()$step()$step(),
            "Conjunction"
        )
        expect_is(
            Sentence("State {STATES} included")$root()$step()$step()$step(),
            "NULL"
        )
        expect_equal(
            Sentence("State {STATES} included")$print(),
            "State {STATES} included"
        )
        expect_equal(
            Sentence("State {STATES} included")$root()$print(),
            "State "
        )
        expect_equal(
            Sentence("State {STATES} included")$root()$step()$print(),
            "{STATES}"
        )
        expect_equal(
            Sentence("State {STATES} included")$root()$step()$step()$print(),
            " included"
        )
        expect_is(
            Sentence("State {STATES} included")$root()$step()$root(),
            "Phrase"
        )
        expect_is(
            Sentence("State {STATES} included")$root()$step()$root()$step(),
            "NULL"
        )
        expect_equal(
            Sentence("State {STATES} included")$root()$step()$root()$print(),
            "STATES"
        )
        expect_is(
            Sentence("State {STATES} included")$root()$step()$root()$getalias(),
            "NULL"
        )
        expect_equal(
            Sentence("State {STATES} included")$root()$step()$root()$getkey(),
            "STATES"
        )
    }
)

test_that(
    "False Start Case Key Only",
    {
        expect_is(
            Sentence("{State {STATES} included"),
            "Sentence"
        )
        expect_is(
            Sentence("{State {STATES} included")$root(),
            "Conjunction"
        )
        expect_is(
            Sentence("{State {STATES} included")$root()$step(),
            "Clause"
        )
        expect_is(
            Sentence("{State {STATES} included")$root()$step()$step(),
            "Conjunction"
        )
        expect_is(
            Sentence("{State {STATES} included")$root()$step()$step()$step(),
            "NULL"
        )
        expect_equal(
            Sentence("{State {STATES} included")$print(),
            "{State {STATES} included"
        )
        expect_equal(
            Sentence("{State {STATES} included")$root()$print(),
            "{State "
        )
        expect_equal(
            Sentence("{State {STATES} included")$root()$step()$print(),
            "{STATES}"
        )
        expect_equal(
            Sentence("{State {STATES} included")$root()$step()$step()$print(),
            " included"
        )
        expect_is(
            Sentence("{State {STATES} included")$root()$step()$root(),
            "Phrase"
        )
        expect_is(
            Sentence("{State {STATES} included")$root()$step()$root()$step(),
            "NULL"
        )
        expect_equal(
            Sentence("{State {STATES} included")$root()$step()$root()$print(),
            "STATES"
        )
    }
)

test_that(
    "Aliased Case Key Only",
    {
        expect_is(
            Sentence("State {ST@STATES} included"),
            "Sentence"
        )
        expect_is(
            Sentence("State {ST@STATES} included")$root(),
            "Conjunction"
        )
        expect_is(
            Sentence("State {ST@STATES} included")$root()$step(),
            "Clause"
        )
        expect_is(
            Sentence("State {ST@STATES} included")$root()$step()$step(),
            "Conjunction"
        )
        expect_is(
            Sentence("State {ST@STATES} included")$root()$step()$step()$step(),
            "NULL"
        )
        expect_equal(
            Sentence("State {ST@STATES} included")$print(),
            "State {ST@STATES} included"
        )
        expect_equal(
            Sentence("State {ST@STATES} included")$root()$print(),
            "State "
        )
        expect_equal(
            Sentence("State {ST@STATES} included")$root()$step()$print(),
            "{ST@STATES}"
        )
        expect_equal(
            Sentence("State {ST@STATES} included")$root()$step()$step()$print(),
            " included"
        )
        expect_is(
            Sentence("State {ST@STATES} included")$root()$step()$root(),
            "Phrase"
        )
        expect_is(
            Sentence("State {ST@STATES} included")$root()$step()$root()$step(),
            "NULL"
        )
        expect_equal(
            Sentence("State {ST@STATES} included")$root()$step()$root()$print(),
            "ST@STATES"
        )
        expect_equal(
            Sentence(
                "State {ST@STATES} included"
            )$root()$step()$root()$getalias(),
            "ST"
        )
        expect_equal(
            Sentence(
                "State {ST@STATES} included"
            )$root()$step()$root()$getkey(),
            "STATES"
        )
    }
)

test_that(
    "Blank Alias Case Key Only",
    {
        expect_is(
            Sentence("State {@STATES} included"),
            "Sentence"
        )
        expect_is(
            Sentence("State {@STATES} included")$root(),
            "Conjunction"
        )
        expect_is(
            Sentence("State {@STATES} included")$root()$step(),
            "NULL"
        )
        expect_equal(
            Sentence("State {@STATES} included")$print(),
            "State {@STATES} included"
        )
        expect_equal(
            Sentence("State {@STATES} included")$root()$print(),
            "State {@STATES} included"
        )
    }
)

test_that(
    "Double Alias Case Key Only",
    {
        expect_is(
            Sentence("State {ST@STA@STATES} included"),
            "Sentence"
        )
        expect_is(
            Sentence("State {ST@STA@STATES} included")$root(),
            "Conjunction"
        )
        expect_is(
            Sentence("State {ST@STA@STATES} included")$root()$step(),
            "NULL"
        )
        expect_equal(
            Sentence("State {ST@STA@STATES} included")$print(),
            "State {ST@STA@STATES} included"
        )
        expect_equal(
            Sentence("State {ST@STA@STATES} included")$root()$print(),
            "State {ST@STA@STATES} included"
        )
    }
)
